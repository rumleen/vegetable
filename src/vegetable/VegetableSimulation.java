/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package vegetable;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class VegetableSimulation {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        VegetableFactory vegFactory = new VegetableFactory();
        Vegetable veg1 = vegFactory.getVegetable("Beet");
        veg1.setColor("pink");
        veg1.setName("Beet");
        veg1.setSize(3);
        boolean ripe1 = veg1.isRipe(true);

        Vegetable veg2 = vegFactory.getVegetable("Beet");
        veg2.setColor("pink");
        veg2.setName("Beet");
        veg2.setSize(4);
        boolean ripe2 = veg2.isRipe(false);

        Vegetable veg3 = vegFactory.getVegetable("Carrot");
        veg3.setColor("red");
        veg3.setName("Carrot");
        veg3.setSize(4);
        boolean ripe3 = veg3.isRipe(true);

        Vegetable veg4 = vegFactory.getVegetable("Carrot");
        veg4.setColor("red");
        veg4.setName("Carrot");
        veg4.setSize(6);
        boolean ripe4 = veg4.isRipe(false);

        System.out.println("Vegetable 1 :" + veg1.getName() + " Ripe: "+ripe1);
        System.out.println("Vegetable 2 :" + veg2.getName() + " Ripe: "+ripe2);
        System.out.println("Vegetable 3 :" + veg3.getName() + " Ripe: "+ripe3);
        System.out.println("Vegetable 4 :" + veg4.getName() + " Ripe: "+ripe4);

    }

}
