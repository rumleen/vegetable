/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package vegetable;
import java.util.ArrayList;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class TestVegetable {
    public static void main(String args[]){
        ArrayList<Vegetable> veggies = new ArrayList();
        veggies.add(new Beet("pink", 5, "Beet"));
        veggies.add(new Carrot("red", 3, "Carrot"));
        for (Vegetable veggie : veggies){
            System.out.println(veggie.getName() + "is not Ripe");
        }
    }
}
