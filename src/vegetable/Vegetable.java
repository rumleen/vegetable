/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package vegetable;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public abstract class Vegetable {

   private String colour;
   private int size;
   private String name;
	   
   Vegetable(String colour, int size, String name){
       this.colour = colour;
       this.size = size;
       this.name = name;
   }
   public Vegetable() {
        // TODO Auto-generated constructor stub
}
abstract boolean isRipe(boolean ripe);

   public String getColour(){
       return this.colour;
   }

   public void setColor(String color)
   {
           this.colour = color;
   }
   public int getSize(){
       return this.size;
   }
   public void setSize(int size)
   {
           this.size = size;
   }

   public String getName(){
       return this.name;
   }
   public void setName(String name)
   {
           this.name = name;
   }
	
}
