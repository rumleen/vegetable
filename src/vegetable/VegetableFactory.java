/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package vegetable;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class VegetableFactory {
     public Vegetable getVegetable(String vegetableType)
    {
        String c = null, n = null;
        int s = 0;
        if(vegetableType == null)
        {
            return null;
        }
        if(vegetableType.equalsIgnoreCase("Beet"))
        {
            return new Beet(c,s,n);
        }
        else if(vegetableType.equalsIgnoreCase("Carrot"))
        {
            return new Carrot(c,s,n);
        }
        return null;
    }
}
