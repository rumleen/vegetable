/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package vegetable;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Carrot extends Vegetable {
    public boolean ripe;
    public Carrot (String colour, int size, String name){
        super(colour, size, name);
    }
    public Carrot() {
    	super();
	}
@Override

    public boolean isRipe(boolean ripe){
        this.ripe = ripe;
        return ripe;
    }
}
